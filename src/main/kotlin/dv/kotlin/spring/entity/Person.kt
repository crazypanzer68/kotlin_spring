package dv.kotlin.spring.entity

data class Person(var name: String, var surname: String, var age: Int)

