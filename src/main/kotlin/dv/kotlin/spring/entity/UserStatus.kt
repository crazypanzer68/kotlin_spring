package dv.kotlin.spring.entity

enum class UserStatus {
    PENDING, ACTIVE, NOTACTIVE, DELETED
}