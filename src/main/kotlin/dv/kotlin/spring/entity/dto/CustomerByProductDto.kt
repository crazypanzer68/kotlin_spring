package dv.kotlin.spring.entity.dto

data class CustomerByProductDto(
        var customer: CustomerDto? = null
)