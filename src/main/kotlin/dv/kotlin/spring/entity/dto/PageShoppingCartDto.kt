package dv.kotlin.spring.entity.dto

data class PageShoppingCartDto(
        var totalPage: Int? = null,
        var totalElement: Long? = null,
        var shoppingCarts: List<ShoppingCartDto> = mutableListOf()
)