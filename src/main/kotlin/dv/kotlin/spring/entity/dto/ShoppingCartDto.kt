package dv.kotlin.spring.entity.dto

import dv.kotlin.spring.entity.ShoppingCartStatus

data class ShoppingCartDto(
        var id: Long? = null,
        var status: ShoppingCartStatus? = ShoppingCartStatus.WAIT,
        var selectedProductsC: List<SelectedProductDto>? = null,
        var customerC: CustomerDto? = null
)