package dv.kotlin.spring.entity.dto

data class SelectedProductInCartDto(var id: Long? = null,
                                    var name: String? = null,
                                    var description: String? = null,
                                    var quantity: Int? = null)