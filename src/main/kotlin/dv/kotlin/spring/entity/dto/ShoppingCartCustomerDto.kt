package dv.kotlin.spring.entity.dto

import dv.kotlin.spring.entity.Address

data class ShoppingCartCustomerDto(var customer: String? = null,
                                   var defaultAddress: Address? = null,
                                   var products: List<SelectedProductInCartDto>? = null)