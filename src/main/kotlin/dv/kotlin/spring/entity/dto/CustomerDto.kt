package dv.kotlin.spring.entity.dto

import dv.kotlin.spring.entity.Address
import dv.kotlin.spring.entity.UserStatus
import dv.kotlin.spring.security.entity.Authority

//data class CustomerDto(
//        var name: String? = null,
//        var email: String? = null,
//        var userStatus: UserStatus? = UserStatus.PENDING,
//        var defaultAddress: Address? = null,
//        var id: Long? = null
//
//)

data class CustomerDto(
        var name: String? = null,
        var email: String? = null,
        var username: String? = null,
        var defaultAddress: Address? = null,
        var authorities: List<AuthorityDto> = mutableListOf()
)
