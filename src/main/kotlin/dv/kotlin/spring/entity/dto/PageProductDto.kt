package dv.kotlin.spring.entity.dto

data class PageProductDto(var totalPages: Int? = null,
                          var totalElements: Long? = null,
                          var products: List<ProductDto> = mutableListOf())