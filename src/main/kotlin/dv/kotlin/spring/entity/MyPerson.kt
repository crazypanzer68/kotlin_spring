package dv.kotlin.spring.entity

data class MyPerson(var name: String, var surname: String, var team: String, var number: Int)