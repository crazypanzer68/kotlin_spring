package dv.kotlin.spring.entity

enum class ShoppingCartStatus {
    WAIT,
    CONFIRM,
    PAID,
    SENT,
    RECEIVED
}