package dv.kotlin.spring.util

import dv.kotlin.spring.entity.*
import dv.kotlin.spring.entity.dto.*
import dv.kotlin.spring.security.entity.Authority
import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings
import org.mapstruct.factory.Mappers

@Mapper(componentModel = "spring")
interface MapperUtil {
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }

    @Mappings(
            Mapping(source = "manufacturer", target = "manu")
    )

    fun mapProductDto(product: Product?): ProductDto?

    fun mapProductDto(products: List<Product>): List<ProductDto>

    @InheritInverseConfiguration
    fun mapProductDto(productDto: ProductDto): Product

    fun mapManufacturerDto(manufacturers: List<Manufacturer>): List<ManufacturerDto>

    fun mapManufacturer(manu: Manufacturer): ManufacturerDto

    @InheritInverseConfiguration
    fun mapManufacturer(manu: ManufacturerDto): Manufacturer

    @Mappings(
            Mapping(source = "customer.jwtUser.username", target = "username"),
            Mapping(source = "customer.jwtUser.authorities", target = "authorities")
    )

    fun mapCustomerDto(customer: Customer?): CustomerDto

    fun mapCustomerDto(customers: List<Customer>): List<CustomerDto>

    @InheritInverseConfiguration
    fun mapCustomerDto(customerDto: CustomerDto): Customer

    fun mapAuthority(authority: Authority): AuthorityDto

    fun mapAuthority(authority: List<Authority>): List<AuthorityDto>

    fun mapAddress(address: Address): AddressDto

    @Mappings(
            Mapping(source = "customer", target = "customerC"),
            Mapping(source = "selectedProducts", target = "selectedProductsC")
    )
    fun mapShoppingCartDto(shoppingCart: ShoppingCart): ShoppingCartDto

    fun mapShoppingCartDto(shoppingCarts: List<ShoppingCart>): List<ShoppingCartDto>

    fun mapSelectedProduct(selectedProducts: SelectedProduct): SelectedProductDto

    fun mapSelectedProduct(selectedProducts: List<SelectedProduct>): List<SelectedProductDto>

    fun mapCustomerProduct(shoppingCart: ShoppingCart): CustomerByProductDto

    fun mapCustomerProduct(ShoppingCart: List<ShoppingCart>): List<CustomerByProductDto>

    @InheritInverseConfiguration
    fun mapAddress(address: AddressDto): Address

}