package dv.kotlin.spring.service

import dv.kotlin.spring.dao.AddressDao
import dv.kotlin.spring.dao.CustomerDao
import dv.kotlin.spring.entity.Customer
import dv.kotlin.spring.entity.ShoppingCart
import dv.kotlin.spring.entity.UserStatus
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class CustomerServiceImpl : CustomerService {
    @Transactional
    override fun remove(id: Long): Customer? {
        val customer = customerDao.findById(id)
        customer?.isDeleted = true
        return customer
    }

    @Transactional
    override fun save(addressId: Long, customer: Customer): Customer {
        var address = addressDao.findById(addressId)
        val customer = customerDao.save(customer)
        customer.defaultAddress = address
        return customer
    }

    @Transactional
    override fun save(customer: Customer): Customer {
        var address = customer.defaultAddress?.let { addressDao.save(it) }
        val customer = customerDao.save(customer)
        return customer
    }

    @Autowired
    lateinit var addressDao: AddressDao

    override fun getCustomerByStatus(status: Enum<UserStatus>): List<Customer> {
        return customerDao.getCustomerByStatus(status)
    }

    override fun getCustomerByProvince(province: String): List<Customer> {
        return customerDao.getCustomerByProvince(province)
    }

    override fun getCustomerByPartialNameAndEmail(name: String, email: String): List<Customer> {
        return customerDao.getCustomerByPartialNameAndEmail(name, email)
    }

    override fun getCustomerByPartialName(name: String): List<Customer> {
        return customerDao.getCustomerByPartialName(name)
    }

    @Autowired
    lateinit var customerDao: CustomerDao

    override fun getCustomers(): List<Customer> {
        return customerDao.getCustomers()
    }

    override fun getCustomerByName(name: String): Customer? {
        return customerDao.getCustomerByName(name)
    }
}