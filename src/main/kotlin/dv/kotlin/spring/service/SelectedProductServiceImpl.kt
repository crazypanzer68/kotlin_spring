package dv.kotlin.spring.service

import dv.kotlin.spring.dao.SelectedProductDao
import dv.kotlin.spring.entity.SelectedProduct
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class SelectedProductServiceImpl : SelectedProductService {
    override fun getProductWithPage(name: String, page: Int, pageSize: Int): Page<SelectedProduct> {
        return selectedProductDao.getProductWithPage(name, page, pageSize)
    }

    @Autowired
    lateinit var selectedProductDao: SelectedProductDao
}