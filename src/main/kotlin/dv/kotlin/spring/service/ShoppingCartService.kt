package dv.kotlin.spring.service

import dv.kotlin.spring.entity.ShoppingCart
import dv.kotlin.spring.entity.dto.ShoppingCartCustomerDto
import org.springframework.data.domain.Page

interface ShoppingCartService {
    fun getShoppingCarts(): List<ShoppingCart>
    fun getShoppingCartWithPage(page: Int, pageSize: Int): Page<ShoppingCart>
    fun getShoppingCartPartialNameWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart>
    fun getCustomerByProduct(name: String): List<ShoppingCart>
    fun save(customerId: Long, shoppingCartCustomerDto: ShoppingCartCustomerDto): ShoppingCart

}