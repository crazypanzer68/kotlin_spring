package dv.kotlin.spring.service

import dv.kotlin.spring.dao.AddressDao
import dv.kotlin.spring.entity.Address
import dv.kotlin.spring.entity.dto.AddressDto
import dv.kotlin.spring.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class AddressServiceImpl : AddressService {
    @Autowired
    lateinit var addressDao: AddressDao

    override fun save(address: AddressDto): Address {
        val address = MapperUtil.INSTANCE.mapAddress(address)
        return addressDao.save(address)
    }

}