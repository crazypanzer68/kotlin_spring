package dv.kotlin.spring.service

import dv.kotlin.spring.entity.SelectedProduct
import org.springframework.data.domain.Page

interface SelectedProductService {
    fun getProductWithPage(name: String, page: Int, pageSize: Int): Page<SelectedProduct>
}