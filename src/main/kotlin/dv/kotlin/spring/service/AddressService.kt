package dv.kotlin.spring.service

import dv.kotlin.spring.entity.Address
import dv.kotlin.spring.entity.dto.AddressDto

interface AddressService {
    fun save(address: AddressDto): Address
}