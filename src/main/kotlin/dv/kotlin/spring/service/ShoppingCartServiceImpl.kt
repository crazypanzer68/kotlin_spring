package dv.kotlin.spring.service

import dv.kotlin.spring.dao.CustomerDao
import dv.kotlin.spring.dao.ProductDao
import dv.kotlin.spring.dao.SelectedProductDao
import dv.kotlin.spring.dao.ShoppingCartDao
import dv.kotlin.spring.entity.SelectedProduct
import dv.kotlin.spring.entity.ShoppingCart
import dv.kotlin.spring.entity.ShoppingCartStatus
import dv.kotlin.spring.entity.dto.ShoppingCartCustomerDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class ShoppingCartServiceImpl : ShoppingCartService {
    @Autowired
    lateinit var customerDao: CustomerDao
    @Autowired
    lateinit var productDao: ProductDao
    @Autowired
    lateinit var selectedProductDao: SelectedProductDao

    @Transactional
    override fun save(customerId: Long, shoppingCartCustomerDto: ShoppingCartCustomerDto): ShoppingCart {
        val customer = customerDao.findById(customerId)
        val shoppingCart = ShoppingCart(status = ShoppingCartStatus.WAIT)
        for (each in shoppingCartCustomerDto.products!!) {
            var product = each.id?.let { productDao.findById(it) }
            var selectedProduct = SelectedProduct(each.quantity)
            selectedProduct.product = product
            selectedProductDao.save(selectedProduct)
            shoppingCart.selectedProducts.add(selectedProduct)
        }
        shoppingCart.customer = customer
        shoppingCartDao.save(shoppingCart)
        return shoppingCart
    }

    override fun getCustomerByProduct(name: String): List<ShoppingCart> {
        return shoppingCartDao.getCustomerByProduct(name)
    }

    override fun getShoppingCartPartialNameWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingCartDao.getShoppingCartPartialNameWithPage(name, page, pageSize)
    }

    override fun getShoppingCartWithPage(page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingCartDao.getShoppingCartWithPage(page, pageSize)
    }

    @Autowired
    lateinit var shoppingCartDao: ShoppingCartDao

    override fun getShoppingCarts(): List<ShoppingCart> {
        return shoppingCartDao.getShoppingCarts()
    }
}