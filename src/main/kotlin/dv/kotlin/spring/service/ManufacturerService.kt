package dv.kotlin.spring.service

import dv.kotlin.spring.entity.Manufacturer
import dv.kotlin.spring.entity.dto.ManufacturerDto

interface ManufacturerService {
    fun getManufacturers(): List<Manufacturer>
    fun save(manu: ManufacturerDto): Manufacturer
}
