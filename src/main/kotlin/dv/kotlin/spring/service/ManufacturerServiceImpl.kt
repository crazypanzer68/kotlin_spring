package dv.kotlin.spring.service

import dv.kotlin.spring.dao.ManufacturerDao
import dv.kotlin.spring.entity.Manufacturer
import dv.kotlin.spring.entity.dto.ManufacturerDto
import dv.kotlin.spring.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ManufacturerServiceImpl : ManufacturerService {

    override fun save(manu: ManufacturerDto): Manufacturer {
        val manufacturer = MapperUtil.INSTANCE.mapManufacturer(manu)
        return manufacturerDao.save(manufacturer)
    }

    @Autowired
    lateinit var manufacturerDao: ManufacturerDao

    override fun getManufacturers(): List<Manufacturer> {
        return manufacturerDao.getManufacturers()
    }
}