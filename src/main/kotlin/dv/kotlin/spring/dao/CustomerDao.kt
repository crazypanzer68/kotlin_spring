package dv.kotlin.spring.dao

import dv.kotlin.spring.entity.Customer
import dv.kotlin.spring.entity.UserStatus

interface CustomerDao{
    fun getCustomers(): List<Customer>
    fun getCustomerByName(name:String):Customer?
    fun getCustomerByPartialName(name: String): List<Customer>
    fun getCustomerByPartialNameAndEmail(name: String, email: String): List<Customer>
    fun getCustomerByProvince(province: String): List<Customer>
    fun getCustomerByStatus(status: Enum<UserStatus>): List<Customer>
    fun save(customer: Customer): Customer
    fun findById(id: Long): Customer?
}