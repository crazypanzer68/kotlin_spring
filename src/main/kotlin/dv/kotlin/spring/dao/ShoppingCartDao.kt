package dv.kotlin.spring.dao

import dv.kotlin.spring.entity.ShoppingCart
import org.springframework.data.domain.Page

interface ShoppingCartDao {
    fun getShoppingCarts(): List<ShoppingCart>
    fun getShoppingCartWithPage(page: Int, pageSize: Int): Page<ShoppingCart>
    fun getShoppingCartPartialNameWithPage(name: String, page: Int, pageSize: Int): Page<ShoppingCart>
    fun getCustomerByProduct(name: String): List<ShoppingCart>
    fun save(shoppingCart: ShoppingCart): ShoppingCart
    fun getShoppingCartByCustomerId(customerId: Long): ShoppingCart
    fun getShoppingCartByProductName(name: String): List<ShoppingCart>
}