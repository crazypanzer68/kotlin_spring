package dv.kotlin.spring.dao

import dv.kotlin.spring.entity.Manufacturer
import dv.kotlin.spring.repository.ManufacturerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class ManufacturerDaoDBImpl : ManufacturerDao {
    override fun findById(id: Long): Manufacturer? {
        return manufacturerRepository.findById(id).orElse(null)
    }

    override fun getManufacturers(): List<Manufacturer> {
        return manufacturerRepository.findAll().filterIsInstance(Manufacturer::class.java)
    }

    @Autowired
    lateinit var manufacturerRepository: ManufacturerRepository

    override fun save(manufacturer: Manufacturer): Manufacturer {
        return manufacturerRepository.save(manufacturer)
    }
}