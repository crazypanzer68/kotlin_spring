package dv.kotlin.spring.dao

import dv.kotlin.spring.entity.SelectedProduct
import org.springframework.data.domain.Page

interface SelectedProductDao {
    fun getProductWithPage(name: String, page: Int, pageSize: Int): Page<SelectedProduct>
    fun save(selectedProduct: SelectedProduct): SelectedProduct
}