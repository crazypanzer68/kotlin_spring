package dv.kotlin.spring.dao

import dv.kotlin.spring.entity.Address

interface AddressDao {
    fun save(address: Address): Address
    fun findById(addressId: Long): Address?
}