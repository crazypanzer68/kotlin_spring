//package dv.kotlin.spring.dao
//
//import dv.kotlin.spring.entity.Manufacturer
//import org.springframework.context.annotation.Profile
//import org.springframework.stereotype.Repository
//
//@Profile("db")
//@Repository
//class ManufacturerDaoImpl : ManufacturerDao {
//    override fun getManufacturers(): List<Manufacturer> {
//        return mutableListOf(Manufacturer("Apple", "053123456"),
//                Manufacturer("Samsung", "555666777888"),
//                Manufacturer("CAMT", "0000000"))
//    }
//}