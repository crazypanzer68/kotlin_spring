package dv.kotlin.spring.dao

import dv.kotlin.spring.entity.Address
import dv.kotlin.spring.repository.AddressRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class AddressDaoDBImpl : AddressDao {
    override fun findById(addressId: Long): Address? {
        return addressRepository.findById(addressId).orElse(null)
    }

    @Autowired
    lateinit var addressRepository: AddressRepository

    override fun save(address: Address): Address {
        return addressRepository.save(address)
    }
}