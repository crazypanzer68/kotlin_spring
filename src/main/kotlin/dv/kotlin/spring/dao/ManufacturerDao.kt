package dv.kotlin.spring.dao

import dv.kotlin.spring.entity.Manufacturer

interface ManufacturerDao {
    fun getManufacturers(): List<Manufacturer>
    fun save(manufacturer: Manufacturer): Manufacturer
    fun findById(id: Long): Manufacturer?
}