package dv.kotlin.spring.dao

import dv.kotlin.spring.entity.Customer
import dv.kotlin.spring.entity.Product
import dv.kotlin.spring.entity.ShoppingCart
import dv.kotlin.spring.entity.UserStatus
import dv.kotlin.spring.repository.CustomerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class CustomerDaoDBImpl : CustomerDao {
    override fun findById(id: Long): Customer? {
        return customerRepository.findById(id).orElse(null)
    }

    override fun save(customer: Customer): Customer {
        return customerRepository.save(customer)
    }

    override fun getCustomerByStatus(status: Enum<UserStatus>): List<Customer> {
        return customerRepository.findByUserStatus(status)
    }

    override fun getCustomerByProvince(province: String): List<Customer> {
        return customerRepository.findByDefaultAddress_ProvinceContainingIgnoreCase(province)
    }

    override fun getCustomerByPartialNameAndEmail(name: String, email: String): List<Customer> {
        return customerRepository.findByNameContainingIgnoreCaseOrEmailContainingIgnoreCase(name, email)
    }

    override fun getCustomerByPartialName(name: String): List<Customer> {
        return customerRepository.findByNameContainingIgnoreCase(name)
    }

    @Autowired
    lateinit var customerRepository: CustomerRepository

    override fun getCustomers(): List<Customer> {
        return customerRepository.findByIsDeletedIsFalse()
    }

    override fun getCustomerByName(name: String): Customer? {
        return customerRepository.findByName((name))
    }
}