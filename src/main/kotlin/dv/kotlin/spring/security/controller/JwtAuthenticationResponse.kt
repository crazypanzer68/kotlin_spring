package dv.kotlin.spring.security.controller

data class JwtAuthenticationResponse(
        var token: String? = null
)