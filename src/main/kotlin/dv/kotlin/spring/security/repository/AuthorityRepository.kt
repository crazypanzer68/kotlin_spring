package dv.kotlin.spring.security.repository

import dv.kotlin.spring.security.entity.Authority
import dv.kotlin.spring.security.entity.AuthorityName
import org.springframework.data.repository.CrudRepository

interface AuthorityRepository : CrudRepository<Authority, Long> {
    fun findByName(input: AuthorityName): Authority
}