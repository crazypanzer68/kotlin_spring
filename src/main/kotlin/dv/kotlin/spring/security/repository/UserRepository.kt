package dv.kotlin.spring.security.repository

import dv.kotlin.spring.security.entity.JwtUser
import org.springframework.data.repository.CrudRepository

interface UserRepository : CrudRepository<JwtUser, Long> {
    fun findByUsername(username: String): JwtUser
}