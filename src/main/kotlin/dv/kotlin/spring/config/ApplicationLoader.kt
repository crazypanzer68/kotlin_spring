package dv.kotlin.spring.config

import dv.kotlin.spring.entity.*
import dv.kotlin.spring.repository.*
import dv.kotlin.spring.security.entity.Authority
import dv.kotlin.spring.security.entity.AuthorityName
import dv.kotlin.spring.security.entity.JwtUser
import dv.kotlin.spring.security.repository.AuthorityRepository
import dv.kotlin.spring.security.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Component
import javax.transaction.Transactional

@Component
class ApplicationLoader : ApplicationRunner {
    @Autowired
    lateinit var manufacturerRepository: ManufacturerRepository
    @Autowired
    lateinit var productRepository: ProductRepository
    @Autowired
    lateinit var addressRepository: AddressRepository
    @Autowired
    lateinit var customerRepository: CustomerRepository
    @Autowired
    lateinit var selectedProductRepository: SelectedProductRepository
    @Autowired
    lateinit var shoppingCartRepository: ShoppingCartRepository
    @Autowired
    lateinit var dataLoader: DataLoader
    @Autowired
    lateinit var authorityRepository: AuthorityRepository
    @Autowired
    lateinit var userRepository: UserRepository

    @Transactional
    override fun run(args: ApplicationArguments?) {
        var mau1 = manufacturerRepository.save(Manufacturer("CAMT", "0000000"))
        var mau2 = manufacturerRepository.save(Manufacturer("Apple", "053123456"))
        var mau3 = manufacturerRepository.save(Manufacturer("Samsung", "555666777888"))
        var product1 = productRepository.save(Product("CAMT",
                "The best College in CMU", 0.0, 1,
                "http://www.camt.cmu.ac.th/th/images/logo.jpg"))
        mau1.products.add(product1)
        product1.manufacturer = mau1
        var product2 = productRepository.save(Product("Prayuth",
                "The best PM ever", 1.00, 1,
                "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Prayut_Chan-o-cha_%28cropped%29_2016.jpg/200px-Prayut_Chan-o-cha_%28cropped%29_2016.jpg"))
        mau1.products.add(product2)
        product2.manufacturer = mau1
        var product3 = productRepository.save(Product("iPhone",
                "It's a phone", 28000.00, 20,
                "https://www.jaymartstore.com/Products/iPhone-X-64GB-Space-Grey--1140900010552--4724"))
        mau2.products.add(product3)
        product3.manufacturer = mau2
        var product4 = productRepository.save(Product("Note 9",
                "Other Iphone", 28001.0, 10,
                "http://dynamic-cdn.eggdigital.com/e56zBiUt1.jpg"))
        mau3.products.add(product4)
        product4.manufacturer = mau3
        var address1 = addressRepository.save(Address("ถนนอนุสาวรีย์ประชาธิปไตย",
                "แขวง ดินสอ", "เขตดุสิต", "กรุงเทพ", "10123"))
        var address2 = addressRepository.save(Address("239 มหาวิทยาลัยเชียงใหม่",
                "ต.สุเทพ", "อ.เมือง", "จ.เชียงใหม่", "50200"))
        var address3 = addressRepository.save(Address("ซักที่บนโลก",
                "ต.สุขสันต์", "อ.ในเมือง", "จ.ขอนแก่น", "12457"))
        var customer1 = customerRepository.save(Customer("Lung", "pm@go.th", UserStatus.ACTIVE))
        customer1.defaultAddress = address1
        var customer2 = customerRepository.save(Customer("ชัชชาติ", "chut@taopoon.com", UserStatus.ACTIVE))
        customer2.defaultAddress = address2
        var customer3 = customerRepository.save(Customer("ธนาธร", "thanathorn@life.com", UserStatus.PENDING))
        customer3.defaultAddress = address3
        //        var shopCart1 = shoppingCartRepository.save(ShoppingCart(ShoppingCartStatus.SENT))
//        shopCart1.customer = customer1
//        var selected1 = selectedProductRepository.save(SelectedProduct(product3, 4))
//        var selected2 = selectedProductRepository.save(SelectedProduct(product2, 1))
//        shopCart1.selectedProducts.add(selected1)
//        shopCart1.selectedProducts.add(selected2)
//        var shopCart2 = shoppingCartRepository.save(ShoppingCart(ShoppingCartStatus.WAIT))
//        shopCart2.customer = customer2
//        var selected3 = selectedProductRepository.save(SelectedProduct(product2, 1))
//        var selected4 = selectedProductRepository.save(SelectedProduct(product1, 1))
//        var selected5 = selectedProductRepository.save(SelectedProduct(product4, 2))
//        shopCart2.selectedProducts.add(selected3)
//        shopCart2.selectedProducts.add(selected4)
//        shopCart2.selectedProducts.add(selected5)
        @Transactional
        fun loadUsernameAndPassword() {
            val auth1 = Authority(name = AuthorityName.ROLE_ADMIN)
            val auth2 = Authority(name = AuthorityName.ROLE_CUSTOMER)
            val auth3 = Authority(name = AuthorityName.ROLE_GENERAL)
            authorityRepository.save(auth1)
            authorityRepository.save(auth2)
            authorityRepository.save(auth3)
            val encoder = BCryptPasswordEncoder()
            val cust1 = Customer(name = "สมชาติ", email = "a@b.com")
            val custJwt = JwtUser(
                    username = "customer",
                    password = encoder.encode("password"),
                    email = cust1.email,
                    enabled = true,
                    firstname = cust1.name,
                    lastname = "unknown"
            )
            customerRepository.save(cust1)
            userRepository.save(custJwt)
            cust1.jwtUser = custJwt
            custJwt.user = cust1
            custJwt.authorities.add(auth2)
            custJwt.authorities.add(auth3)
        }
        dataLoader.loadData()
        loadUsernameAndPassword()
    }
}