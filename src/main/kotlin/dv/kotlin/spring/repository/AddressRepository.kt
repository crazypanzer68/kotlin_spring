package dv.kotlin.spring.repository

import org.springframework.data.repository.CrudRepository

interface AddressRepository : CrudRepository<dv.kotlin.spring.entity.Address, Long>