package dv.kotlin.spring.repository

import dv.kotlin.spring.entity.ShoppingCart
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.CrudRepository

interface ShoppingCartRepository : CrudRepository<ShoppingCart, Long> {
    fun findAll(page: Pageable): Page<ShoppingCart>
    fun findBySelectedProducts_Product_NameContainingIgnoreCase(name: String, page: Pageable): Page<ShoppingCart>
    fun findBySelectedProducts_Product_NameContainingIgnoreCase(name: String):List<ShoppingCart>
    fun findByCustomer_Id(customerId: Long): ShoppingCart
}