package dv.kotlin.spring.repository

import dv.kotlin.spring.entity.Manufacturer
import org.springframework.data.repository.CrudRepository

interface ManufacturerRepository : CrudRepository<Manufacturer, Long> {
    fun findByName(name: String): Manufacturer

//    fun save(manufacturer: Manufacturer):Manufacturer
}