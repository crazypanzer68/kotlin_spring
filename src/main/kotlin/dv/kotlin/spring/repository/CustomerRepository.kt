package dv.kotlin.spring.repository

import dv.kotlin.spring.entity.Customer
import dv.kotlin.spring.entity.ShoppingCart
import dv.kotlin.spring.entity.UserStatus
import org.springframework.data.repository.CrudRepository

interface CustomerRepository : CrudRepository<Customer, Long> {
    fun findByName(name: String): Customer?
    fun findByNameContainingIgnoreCase(name: String): List<Customer>
    fun findByNameContainingIgnoreCaseOrEmailContainingIgnoreCase(name:String,email:String):List<Customer>
    fun findByDefaultAddress_ProvinceContainingIgnoreCase(province: String): List<Customer>
    fun findByUserStatus(status: Enum<UserStatus>): List<Customer>
    fun findByIsDeletedIsFalse():List<Customer>
}