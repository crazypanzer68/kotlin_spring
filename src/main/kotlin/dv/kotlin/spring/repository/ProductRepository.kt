package dv.kotlin.spring.repository

import dv.kotlin.spring.entity.Product
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.CrudRepository


interface ProductRepository : CrudRepository<Product, Long> {
    fun findByName(name: String): Product?
    //    fun findByNameContaining(name:String):List<Product>
    fun findByNameContainingIgnoreCase(name: String): List<Product>
    fun findByNameContainingIgnoreCaseOrDescriptionContainingIgnoreCase(name: String, desc: String): List<Product>
    fun findByManufacturer_NameContainingIgnoreCase(name: String): List<Product>
    fun findByNameContainingIgnoreCase(name: String, page: Pageable): Page<Product>
    fun findByIsDeletedIsFalse():List<Product>
}