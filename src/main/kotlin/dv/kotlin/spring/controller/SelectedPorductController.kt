package dv.kotlin.spring.controller

import dv.kotlin.spring.entity.dto.PageSelectedProductDto
import dv.kotlin.spring.service.SelectedProductService
import dv.kotlin.spring.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class SelectedProductController {
    @Autowired
    lateinit var selectedProductService: SelectedProductService

    @GetMapping("/selectedProduct/name")
    fun getProductWithPage(@RequestParam("name") name: String,
                           @RequestParam("page") page: Int,
                           @RequestParam("pageSize") pageSize: Int): ResponseEntity<Any> {
        val output = selectedProductService.getProductWithPage(name, page, pageSize)
        return ResponseEntity.ok(PageSelectedProductDto(totalPage = output.totalPages,
                totalElement = output.totalElements,
                products = MapperUtil.INSTANCE.mapSelectedProduct(output.content)))
    }
}