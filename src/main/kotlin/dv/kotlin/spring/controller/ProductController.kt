package dv.kotlin.spring.controller

import dv.kotlin.spring.entity.dto.PageProductDto
import dv.kotlin.spring.entity.dto.ProductDto
import dv.kotlin.spring.service.ProductService
import dv.kotlin.spring.util.MapperUtil
import org.mapstruct.Mapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class ProductController {
    @Autowired
    lateinit var productService: ProductService

    @GetMapping("/product")
    fun getProducts(): ResponseEntity<Any> {
        val products = productService.getProducts()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapProductDto(products))
    }

    @GetMapping("/product/query")
    fun getProducts(@RequestParam("name") name: String): ResponseEntity<Any> {
        var output = MapperUtil.INSTANCE.mapProductDto((productService.getProductByName(name)))
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @GetMapping("/products/partialQuery")
    fun getProductsPartial(@RequestParam("name") name: String,
                           @RequestParam("desc", required = false) desc: String?): ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapProductDto(
                productService.getProductByPartialNameAndDesc(name, name)
        )
//        val output: List<Any>
//        if (desc == null) {
//            output = MapperUtil.INSTANCE.mapProductDto(productService.getProductByPartialName(name))
//        } else {
//            output = MapperUtil.INSTANCE.mapProductDto(productService.getProductByPartialNameAndDesc(name, desc))
//        }
        return ResponseEntity.ok(output)
    }

    @GetMapping("/product/{manuName}")
    fun getProductByManuName(@PathVariable("manuName") name: String): ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapProductDto(
                productService.getProductManuName(name)
        )
        return ResponseEntity.ok(output)
    }

    @GetMapping("/product/name")
    fun getProductWithPage(@RequestParam("name") name: String,
                           @RequestParam("page") page: Int,
                           @RequestParam("pageSize") pageSize: Int): ResponseEntity<Any> {
        val output = productService.getProductWithPage(name, page, pageSize)
        return ResponseEntity.ok(PageProductDto(totalPages = output.totalPages,
                totalElements = output.totalElements,
                products = MapperUtil.INSTANCE.mapProductDto(output.content)))
    }

    @PostMapping("/product")
    fun addProduct(@RequestBody productDto: ProductDto): ResponseEntity<Any> {
        val output = productService.save(MapperUtil.INSTANCE.mapProductDto(productDto))
        val outputDto = MapperUtil.INSTANCE.mapProductDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @PostMapping("/product/manufacturer/{manuId}")
    fun addProduct(@RequestBody productDto: ProductDto,
                   @PathVariable manuId: Long): ResponseEntity<Any> {
        val output = productService.save(manuId, MapperUtil.INSTANCE.mapProductDto(productDto))
        val outputDto = MapperUtil.INSTANCE.mapProductDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @DeleteMapping("/product/{id}")
    fun deleteProduct(@PathVariable("id") id: Long): ResponseEntity<Any> {
        val product = productService.remove(id)
        val outputProduct = MapperUtil.INSTANCE.mapProductDto(product)
        outputProduct?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("the product id is not found")
    }
}