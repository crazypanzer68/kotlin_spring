//package dv.kotlin.spring.controller
//
//import dv.kotlin.spring.entity.Product
//import org.springframework.http.ResponseEntity
//import org.springframework.web.bind.annotation.*
//
//@RestController
//class WorkController {
//    @GetMapping("/getAppName")
//    fun getAppName(): String {
//        return "assessment"
//    }
//
//    @GetMapping("/product")
//    fun getProduct(): ResponseEntity<Any> {
//        val product = Product("iPhone", "A new telephone", 28000.00, 5)
//        return ResponseEntity.ok(product)
//    }
//
//    @GetMapping("/product/{name}")
//    fun getPathParam(@PathVariable("name") name: String): ResponseEntity<Any> {
//        val product = Product(name, "A new telephone", 28000.00, 5)
//        if (name == "iPhone") {
//            return ResponseEntity.ok(product)
//        } else {
//            return ResponseEntity.notFound().build()
//        }
//    }
//
//    @PostMapping("/setZeroQuantity")
//    fun setZero(@RequestBody product: Product): ResponseEntity<Any> {
//        product.amountInStock = 0
//        return ResponseEntity.ok(product)
//    }
//
//    @PostMapping("/totalPrice")
//    fun totalPrice(@RequestBody product: List<Product>): String {
//        var sum = 0
//        for (i in product.indices) {
////            sum = sum + product[i].price
//        }
//        return "$sum"
//    }
//
//    @PostMapping("/availableProduct")
//    fun availableProduct(@RequestBody product: List<Product>): ResponseEntity<Any> {
//        var output = mutableListOf<Product>()
//        for (i in product.indices) {
//            if (product[i].amountInStock != 0) {
//                output.add(product[i])
//            }
//        }
//        return ResponseEntity.ok(output)
//    }
//}