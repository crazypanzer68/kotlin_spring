package dv.kotlin.spring.controller

import dv.kotlin.spring.entity.dto.PageShoppingCartDto
import dv.kotlin.spring.entity.dto.SelectedProductInCartDto
import dv.kotlin.spring.entity.dto.ShoppingCartCustomerDto
import dv.kotlin.spring.service.ShoppingCartService
import dv.kotlin.spring.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class ShoppingCartController {
    @Autowired
    lateinit var shoppingCartService: ShoppingCartService

    @GetMapping("/shoppingCart")
    fun getAllShoppingCart(): ResponseEntity<Any> {
        val shoppingCarts = shoppingCartService.getShoppingCarts()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapShoppingCartDto(shoppingCarts))
    }

    @GetMapping("/shoppingCart/customer")
    fun getShoppingCartWithPage(@RequestParam("page") page: Int,
                                @RequestParam("pageSize") pageSize: Int): ResponseEntity<Any> {
        val output = shoppingCartService.getShoppingCartWithPage(page, pageSize)
        return ResponseEntity.ok(PageShoppingCartDto(totalPage = output.totalPages,
                totalElement = output.totalElements,
                shoppingCarts = MapperUtil.INSTANCE.mapShoppingCartDto(output.content)))
    }

    @GetMapping("/shoppingCart/product")
    fun getShoppingCartPartialNameWithPage(@RequestParam("product") name: String,
                                           @RequestParam("page") page: Int,
                                           @RequestParam("pageSize") pageSize: Int): ResponseEntity<Any> {
        val output = shoppingCartService.getShoppingCartPartialNameWithPage(name, page, pageSize)
        return ResponseEntity.ok(PageShoppingCartDto(totalPage = output.totalPages,
                totalElement = output.totalElements,
                shoppingCarts = MapperUtil.INSTANCE.mapShoppingCartDto(output.content)))
    }

    @PostMapping("/shoppingCart/{customerId}")
    fun addShoppingCartWithCustomerIdAndProductId(@PathVariable customerId:Long,
                                                  @RequestBody shoppingCartCustomerDto: ShoppingCartCustomerDto):ResponseEntity<Any>{
        val output = MapperUtil.INSTANCE.mapShoppingCartDto(shoppingCartService.save(customerId,shoppingCartCustomerDto))
        var selectedProductInCartDto = mutableListOf<SelectedProductInCartDto>()
        for(each in output.selectedProductsC!!){
            selectedProductInCartDto.add(SelectedProductInCartDto(name = each.product?.name,
                    id = each.product?.id,
                    description =  each.product?.description,
                    quantity = each?.quantity))
        }
        return ResponseEntity.ok(ShoppingCartCustomerDto(customer = output.customerC?.name,
                defaultAddress = output.customerC?.defaultAddress,
                products = selectedProductInCartDto))
    }
}