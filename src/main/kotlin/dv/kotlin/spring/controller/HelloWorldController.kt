package dv.kotlin.spring.controller

import dv.kotlin.spring.entity.MyPerson
import dv.kotlin.spring.entity.Person
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class HelloWorldController {
    @GetMapping("/helloWorld")
    fun getHelloWorld(): String {
        return "HelloWorld"
    }

    @GetMapping("/test")
    fun getTest(): String {
        return "CAMT"
    }

    @GetMapping("/person")
    fun getPerson(): ResponseEntity<Any> {
        val person = Person("somchai", "somrak", 15)
        return ResponseEntity.ok(person)
    }

    @GetMapping("/myPerson")
    fun getmyPerson(): ResponseEntity<Any> {
        val myperson = Person("Pisit", "Ruangpornvisuti", 22)
        return ResponseEntity.ok(myperson)
    }

    @GetMapping("/persons")
    fun getPersons(): ResponseEntity<Any> {
        val person01 = Person("somchai", "somrak", 15)
        val person02 = Person("Prayut", "Chan", 62)
        val person03 = Person("Lung", "Pom", 65)
        val persons = listOf<Person>(person01, person02, person03)
        return ResponseEntity.ok(persons)
    }

    @GetMapping("/myPersons")
    fun getMyPersons(): ResponseEntity<Any> {
        val myperson01 = MyPerson("Osora", "Tsubasa", "Nachansu", 10)
        val myperson02 = MyPerson("Hyuoka", "Kojiro", "meiwa", 9)
        val mypersons = listOf<MyPerson>(myperson01, myperson02)
        return ResponseEntity.ok(mypersons)
    }

    @GetMapping("/params")
    fun getParams(@RequestParam("name") name: String, @RequestParam("surname") surname: String)
            : ResponseEntity<Any> {
        return ResponseEntity.ok("$name $surname")
    }

    @GetMapping("/params/{name}/{surname}/{age}")
    fun getPathParam(@PathVariable("name") name: String,
                     @PathVariable("surname") surname: String,
                     @PathVariable("age") age: Int): ResponseEntity<Any> {
        val person = Person(name, surname, age)
        return ResponseEntity.ok(person)
    }

    @PostMapping("/echo")
    fun echo(@RequestBody person: Person): ResponseEntity<Any> {
        return ResponseEntity.ok(person)
    }
}