package dv.kotlin.spring.controller

import dv.kotlin.spring.entity.UserStatus
import dv.kotlin.spring.entity.dto.CustomerDto
import dv.kotlin.spring.service.CustomerService
import dv.kotlin.spring.service.ShoppingCartService
import dv.kotlin.spring.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class CustomerController {
    @Autowired
    lateinit var customerService: CustomerService
    @Autowired
    lateinit var shoppingCartService: ShoppingCartService

    @GetMapping("/customer")
    fun getAllCustomer(): ResponseEntity<Any> {
        val customers = customerService.getCustomers()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapCustomerDto(customers))
    }

    @GetMapping("/customer/query")
    fun getCustomers(@RequestParam("name") name: String): ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapCustomerDto(customerService.getCustomerByName(name))
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @GetMapping("/customers/partialQuery")
    fun getCustomersPartial(@RequestParam("name") name: String,
                            @RequestParam("email", required = false) email: String?): ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapCustomerDto(
                customerService.getCustomerByPartialNameAndEmail(name, name)
        )
//        val output: List<Any>
//        if (email == null) {
//            output = MapperUtil.INSTANCE.mapCustomerDto(customerService.getCustomerByPartialName(name))
//        }else{
//            output = MapperUtil.INSTANCE.mapCustomerDto(customerService.getCustomerByPartialNameAndEmail(name,email))
//        }
        return ResponseEntity.ok(output)
    }

    @GetMapping("/customer/province")
    fun getCustomerByDefaultAddress(@RequestParam("province") province: String): ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapCustomerDto(
                customerService.getCustomerByProvince(province)
        )
        return ResponseEntity.ok(output)
    }

    @GetMapping("/customer/status")
    fun getCustomerByStatus(@RequestParam("status") status: UserStatus): ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapCustomerDto(
                customerService.getCustomerByStatus(status)
        )
        return ResponseEntity.ok(output)
    }

    @GetMapping("/customer/product")
    fun getCustomerByProduct(@RequestParam("name") name: String): ResponseEntity<Any> {
        val output = MapperUtil.INSTANCE.mapCustomerProduct(
                shoppingCartService.getCustomerByProduct(name)
        )
        return ResponseEntity.ok(output)
    }

    @PostMapping("/customer")
    fun addCustomer(@RequestBody customerDto: CustomerDto): ResponseEntity<Any> {
        val output = customerService.save(MapperUtil.INSTANCE.mapCustomerDto(customerDto))
        val outputDto = MapperUtil.INSTANCE.mapCustomerDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @PostMapping("/customer/{addressId}")
    fun addCustomer(@RequestBody customerDto: CustomerDto,
                    @PathVariable addressId: Long): ResponseEntity<Any> {
        val output = customerService.save(addressId, MapperUtil.INSTANCE.mapCustomerDto(customerDto))
        val outputDto = MapperUtil.INSTANCE.mapCustomerDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @DeleteMapping("/customer/{id}")
    fun deleteCustomer(@PathVariable("id") id: Long): ResponseEntity<Any> {
        val customer = customerService.remove(id)
        val outputCustomer = MapperUtil.INSTANCE.mapCustomerDto(customer)
        outputCustomer?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("the customer id is not found")
    }
}